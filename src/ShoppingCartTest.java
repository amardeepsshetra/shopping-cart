import static org.junit.Assert.*;

import org.junit.Test;

public class ShoppingCartTest {
	/*
	 * TC1: Creating a cart
	 * 		When created, the cart has 0 items 
	 * TC2: Empty the cart
	 *		When empty, the cart has 0 items   
	 * TC3: Adding Products to Cart
	 * 		When a new product is added, the number of items must be incremented  
	 * 		When a new product is added, the new balance must be the sum of the previous balance plus the cost of the new product  
	 * TC4: Removing Products from Cart
	 * 		When an item is removed, the number of items must be decreased  
	 * 		When a product not in the cart is removed, a ProductNotFoundException must be thrown  Hint: insert the call in a try block and put a fail() after the call to removeItem()
	 */
	@Test
	public void testCreateCart() {
		// When created, the cart has 0 items 
		// 1. Create a shopping cart
		ShoppingCart cart = new ShoppingCart();
		
		// 2. Use getItemCount() to check how many items are in cart
		int numItems = cart.getItemCount();
		
		assertEquals(0, numItems);

	}

	@Test
	public void testEmptyCart() {
		// When empty, the cart has 0 items  
		
		// 1. Create a cart  (Known issue that getItmeCount() doesn't work properly)
		ShoppingCart cart = new ShoppingCart();
		// 2. Add things to the cart
		Product p = new Product("coffee", 200);
		cart.addItem(p);
		
		Product p2 = new Product("donut", 100);
		cart.addItem(p2);
		
		// 3. EMPTY the cart
		cart.empty();
		
		// 4. Check the number of items in the cart (expected = 0)
		assertEquals(0, cart.getItemCount());		
	}
	
	@Test
	public void testAddingProductsToCart() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testRemoveProductFromCart() {
		fail("Not yet implemented");
	}
}
