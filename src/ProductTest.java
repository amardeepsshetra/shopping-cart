import static org.junit.Assert.*;

import org.junit.Test;

public class ProductTest {

	/*
	 * TC1: Creating a Product
     *		When you create the product, it has a title and a price
	 *
	 * TC2: Checking if two products are equal
	 * 		Two products are equal if they have the same name.
	 */
	@Test
	public void testCreateProduct() {
		// 1. Create a new Product
		Product p = new Product("chair", 40);
		
		// 2. Check that product name = expected name
		assertEquals("chair",p.getTitle());
		
		// 3. Check that product price = expected price
		assertEquals(40, p.getPrice(), 0.01);
		
	}

	@Test
	public void testTwoProductsEqual() {
		// Two products are equal if they have the same name.
		
		// 1. Create two products with same name
		Product p1 = new Product("shoes", 25);
		Product p2 = new Product("shoes", 3025);
		
		// 2. Use .equals() function to check if they have same name
		assertEquals(true,p1.equals(p2));
	}
}
